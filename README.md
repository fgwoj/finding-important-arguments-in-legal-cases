# Finding Importance In Legal Cases

This is my approach for that particular problem

You can find my paper here [Click Here!](https://git.npaper.cc/fgw/finding-importance-in-legal-cases/-/blob/main/Paper/Argument_Importance_Workshop_Paper.pdf?ref_type=heads)

or you can read my dissertation [Click Here!](https://git.npaper.cc/fgw/finding-importance-in-legal-cases/-/blob/main/Paper/disseration.pdf
)
## Abstract 

This project focuses on creating a method that estimates an argument’s impor- tance that can help automatically predict a given case. The project consists of two parts that involve the extraction of an argument from legal case descriptions that will be collected from the Indian Supreme Court. The second part con- sists of estimating the importance of the arguments, which will use a statistical model for predictions. The software display in the console the final list of the arguments and their estimation in percentage.

My finding indicates some correlation between BERT and TF-IDF with DB- SCAN methods while comparing the frequency of words. Furthermore, the ac- curacy of the extracted arguments is about 67%, and the arguments extracted power has some accurate results.

This project might contribute to the field of legal analytics and might have potential applications in legal education, case prediction, and decision-making support for legal practitioners. However, further research is needed to refine the model and expand its applicability to a broader range of legal contexts.

## Introduction
In this project, I have focused on creating an algorithm that will estimate the importance of an argument that can help automatically predict a given case. The project consists of two parts that involve the extraction of an argument from legal case descriptions that will be collected from the Indian Supreme Court. The second part involves estimating the importance of the arguments, which will use a statistical model for predictions. The software displays in the console the final list of the arguments and their estimation in percentage.

To begin with the project, a couple of fundamental problems are arising. The first problem is how to extract the argument from the legal case description effec- tively and precisely. There are multiple research papers describing new systems to extract the arguments from legal cases and blogs, medical documentation, chats, and any other application that requires data mining.

The second problem is to find the importance of an argument in a legal case document after extracting all arguments. The idea of approaching that problem is to first create a weight for specific keywords that will determine the seriousness of the argument. Using these weights and comparing them with the judge’s verdict decision can determine the probability model, which may determine the importance of another legal case. The algorithm may also check the distance between the arguments and the judge’s verdict, which will help train the model.

In the paper ”On the Role of negative precedent in Legal Outcome Prediction”, the researchers focused on determining positive and negative outcomes, which are equally crucial in legal analysis. However, a critical topic in this project must be considered. They found that in their model, the negative outcome is much harder to predict than the positive outcome. Therefore, the researchers suspect that a ”negative outcome is mostly caused by a judge distinguishing the case from its precedent.” (Valvoda, Cotterell, and Teufel, 2022). Using this paper, I will consider this information during the testing stage.

## Data Collection
The legal cases are collected from the Indian Supreme Court (WorldLII - Cat- egories - Countries - India - Courts and Case-Law 2022), which publishes all legal case descriptions freely in the text format (.txt). The legal case descrip- tions consist of the names of the judge assigned and the individuals involved in the case. Private people cannot be identified from those cases because these cases consist of their surnames without the first name. No sensitive data is in- cluded, such as date of birth, the address where those individuals live and their age.

## Extraction of the arguments 
I used the repository ”Semantic Segmentation of Indian Supreme Court Case Documents” (shounakpaul95 and paheli, 2020). In that paper, the researchers use a deep neural Hierarchical BiLSTM CRF architecture to train a model. ”BiLSTM layer captures the semantic information of the word, and finally uses a CRF layer to predict the label.” (Bhattacharya et al., 2019)

The model was trained by supervised learning. They used 50 legal cases that three senior law students annotated. The advantage of this approach is the quality of labels created by these students. These labels will be much better quality of data, as the students have the necessary knowledge to perform this work, than those produced by someone who does not know about Indian law. In addition, using their model will result in better accuracy when training the model to estimate the importance of arguments.

The model performance is to label documents into seven categories: Facts, Ruliing by Lower Court, Argument, statute, Precedent, Ratio of the decision Ruling by Present Court. The project will use the two most important labels: Facts, and Arguments and ignore the rest. These labels determine the importance of the argument over the other arguments in legal cases. A similar approach was used in the book ”Artificial Intelligence and Legal Analytics” which describes three methods such as Mochales and Moen’s, SMILE or LUIMA systems. (Ash- ley, 2019)

## Estimation of the arguments
I have decided to use two methods of estimating the arguments. The first approach I chose was to use machine learning methods using TF-IDF and DB- SCAN; the second approach was to use deep learning methods using pre-trained model BERT. The main reason for using these two approaches is that they have yet to be used.

### DBSCAN and TF-IDF
The main inspiration for using this approach came from these two repositories, where for the first one, textclusteringDBSCAN(Borah, Maliarenko, and Aleixo, 2020), ”unsupervised linguistic functionalities based on textual fields on your data”. It was essential for me to test some working solutions that might work. Another one was more focused on TF-IDF, where the author, in his README, explains how TF-IDF works. Magic-Of-TFIDF(Gurung, 2020). Using their idea, I can transfer some knowledge into my problem to create novel solution. The advantage of that approach is that it is machine learning focused, which is faster and computationally cheaper than the deep learning approach.

### BERT
The BERT (distilbert-base-uncased) was used as a replacement for TF-IDF which is based on machine model concept. Instead, I have decided to fine-tune BERT model with Indian court documents to estimate the arguments after all using DBSCAN. In this case the advantage of that approach is better efficiency.

Using BERT for argument estimation offers several benefits, making it a suitable choice for this specific task. The reasons why BERT is well-suited for argument estimation:
- BERT is a bidirectional model, so it captures context from both ways so it can understand the input deeper than TF-IDF with DBSCAN which leads to better estimation of the argument, strength and relevance.
- This model was pre-trained on a large corpus of text that can learn in general language understanding. By fine-tuning BERT on smaller task-specific datasets, it might adapt to my requirement of argument estimation. This transfer-leaning approach saves computational resources compared to training a model from scratch, especially when my resources are limited.
- Lastly, BERT is able to handle complex language structures because of its archi- tecture, which is based on transformers that can handle long-range dependencies and complex language structures. This allows for a better understanding of the logical structure and coherence of the arguments.


## Evaluation
To evaluate the results for DBSCAN and TF-IDF and BERT is calculated on the base of this formula. The insipration I took was from book ”Data Mining: Concepts and Techniques” in chapter 2.4.7 ”Cosine Similarity”.(Han, Kamber, and Pei, 2012)

When x = Power of an Argument, b = Cosine Similarity of Argument & Document and c = Total Similarity of All Arguments & Document

$$ x = {b \over c }* 100 $$

The cosine similarity of argument and document measures the angle between two non-zero vectors to indicate the similarity between the vectors. The formula for that is:
$$\(A,B) = {A*B \over ||A|| * ||B||}$$

The reasoning behind using cosine similarity is to measure the similarity between the document, and the arguments by consideration of the angle between their high-dimensional vectors. The angle between the vectors in case if it is small the cosine similarity will be close to 1. This indicates a high degree of similarity. However, in case of the large angle the cosine similarity will be close to 0 which indicates a low degree of similarity.

Represents the magnitudes of the vectors A and B respectively. So, to compute the similarity of all the arguments in relation to the document, the software is summing the cosine similarity of each argument with the document. Dividing the cosine similarity of an individual argument and the document by the total similarity of all arguments and the document represents the proportion of the argument’s similarity in relation to the overall similarity.
At the end we compute percentage of the power for each argument by multiply- ing the argument by 100.
The logic behind this evaluation method is based on cosine similarity and com- putation of the power of each argument which is to determinate the relevance and importance of the arguments within the context of the corresponding doc- ument. Converting the power into the percentages allows for comparison and ranking easier the arguments according to their significance in relation to the document.

Calculation of the cosine similarity between the documents and each argument allows to quantify the extent to which arguments are relation to or alienated to document’s content. So, it can help in identification of the most relevant and important arguments which will contribute to understatement of the document. It also helps in normalization of the importance of the arguments by using the proportion within the total similarity of all arguments in the document. This may allow for fair comparison of arguments and provide clear representation of the relevant importance of each argument within the case.

## Conclusion
In conclusion, the project was successfully implemented the solutions that I have planned before, however the results of that solution need adjustments and improvements especially in data cleaning process. Certainly the project still requires a lot of improvements and adjustments, however, as it is first type of the project like this for this kind of problem, I can proudly say that I have managed to complete this unique task even with lots of troubles during finding the optimal solutions.

The results are not correct because of the noise of the data which requires improvement in this area, there is a need for better formula to determinate the power of the arguments and better models for extracting the arguments with higher accuracy, but I believe, I have used all available resources at this time to complete that project, but with the expansion of the new solutions in NLP area, in couple of years this project will be outdated if not further developed.
Another aspect to consider would be the amount of the training of the model. It is possible to feed model with more documents, with better labels which could improve the accuracy of both extraction and estimation of the arguments.

It is possible to transfer my software into the working API such other people might use it for estimating the importance of the arguments. This apis could be used in popular software such as Word, PDF viewers with add-ons. But there will be need to adjust the results to be outputted into the json format and transferred over the internet.

## References
Ashley, Kevin D (2019). Artificial intelligence and legal analytics : new tools for law practice in the digital age. Cambridge University Press, , Cop.
Bhattacharya, Paheli et al. (2019). “Identification of Rhetorical Roles of Sen- tences in Indian Legal Judgments”. In: CoRR abs/1911.05405. arXiv: 1911 .05405. url: http://arxiv.org/abs/1911.05405.

Borah, Arnab, Mykola Maliarenko, and Danilo Aleixo ( 2020). textCluster- ingDBSCAN : Clustering text using Density Based Spatial Clustering (DB- SCAN) using TF-IDF, FastText, GloVe word vectors. GitHub. url: https: //github.com/arnab64/textclusteringDBSCAN (visited on 04/06/2023).

Copyright, Designs and Patents Act 1988 (1988). url: https://www.legislation.gov.uk/ukpga/1988/48/contents (visited on 10/31/2022).

Data Protection Act 2018 (2018). url: https://www.legislation.gov.uk/ukpga/2018/12/contents (visited on 10/31/2022).

Gurung, Pema (Aug. 2020). Magic of TF-IDF. GitHub. url: https://github.com/pemagrg1/Magic-Of-TFIDF.

Han, Jiawei, Micheline Kamber, and Jian Pei (2012). Data mining : concepts and techniques. Elsevier.

IBM, IBM (2014). Stop word removal. www.ibm.com. url: https://www.ibm.com/docs/en/watson-explorer/11.0.0?topic=analytics-stop-word-r emoval.

shounakpaul95 and paheli (Sept. 2020). Semantic Segmentation of Indian Supreme Court Case Documents. GitHub. url: https://github.com/Law-AI/semantic-segmentation (visited on 10/31/2022).

Valvoda, Josef, Ryan Cotterell, and Simone Teufel (2022). On the Role of Neg- ative Precedent in Legal Outcome Prediction. doi: 10.48550/ARXIV.2208.0 8225. url: https://arxiv.org/abs/2208.08225.

Verma, Amit and Tony Rollett (2023). Lecture 11 -Clustering. url: http://pajarito.materials.cmu.edu/Data_Analytics-lectures/27737-Clusteri ng-L11-24Mar21.pdf (visited on 05/12/2023).

WorldLII - Categories - Countries - India - Courts and Case-Law (2022). www.worldlii.org. url: http://www.worldlii.org/catalog/2179.html (visited on 10/31/2022).

## License
Creative Commons license Attribution 4.0 International (CC BY 4.0))
